import sys
sys.path.insert(1, 'tools/triage')
sys.path.insert(1, 'tools/utils')

from datetime import datetime

from gitea_utils import (
    gitea_json_issue_events_filter,
    gitea_json_issues_search,
)
import git_log
import os

def compile_list() -> None:
    since = datetime(2024, 12, 1)
    before = datetime(2025, 2, 1)

    labels = f"Module/User Interface"
    issues_json = gitea_json_issues_search(
        type="issues",
        state="closed",
        labels=labels,
        verbose=True,
        since=f"{since.isoformat()}Z",
        before=f"{before.isoformat()}Z",
    )

    # Things are a bit messy here, could use some refactoring :)

    # Doesn't contain high-priority ones
    resolved_bugs = list()
    # In days
    added_age_resolved_bugs = 0
    # In days
    oldest_resolved_bug_age = 0
    other_bugs = list()
    # In days
    added_age_other_bugs = 0
    # In days
    oldest_other_bug_age = 0
    non_bugs = list()
    # In days
    added_age_non_bugs = 0
    # In days
    oldest_non_bug_age = 0
    resolved_high_priority_bugs = list()
    other_high_priority_bugs = list()
    count = 0
    for issue in issues_json:
        fullname = issue["repository"]["full_name"]
        number = issue["number"]
        # For faster testing.
        # if count >= 10:
        #     break
        count += 1

        issue_events = gitea_json_issue_events_filter(
            f"{fullname}/issues/{number}",
            date_start=since,
            date_end=before,
            event_type={"close", "reopen", "commit_ref"}
        )

        if not issue_events:
            continue
        # Skip issues were that were reopened, and not closed again.
        if issue_events[-1]['type'] == "reopen":
            continue
        reopened = False
        # Check if the issue was re-opened and not closed again.
        for event in reversed(issue_events):
            if event['type'] == "close":
                break
            if event['type'] == "reopen":
                reopened = True

        if reopened:
            continue

        commits = list()
        for event in issue_events:
            if event["type"] == "commit_ref":
                commit_sha = event["ref_commit_sha"]
                if commit_sha:
                    commits.append(git_log.GitCommit(commit_sha, os.path.join(os.getcwd(), ".git")))

        age = before.date() - datetime.fromisoformat(issue["created_at"]).date()

        labels = [label["name"] for label in issue["labels"]]
        if "Type/Bug" in labels:
            if "Status/Resolved" in labels:
                if "Severity/High" in labels:
                    resolved_high_priority_bugs.append((issue, commits))
                else:
                    resolved_bugs.append((issue, commits))
                added_age_resolved_bugs += age.days
                oldest_resolved_bug_age = max(oldest_resolved_bug_age, age.days)
            else:
                if "Severity/High" in labels:
                    other_high_priority_bugs.append((issue, commits))
                else:
                    other_bugs.append((issue, commits))
                added_age_other_bugs += age.days
                oldest_other_bug_age = max(oldest_other_bug_age, age.days)
        else:
            non_bugs.append((issue, commits))
            added_age_non_bugs += age.days
            oldest_non_bug_age = max(oldest_non_bug_age, age.days)

    def print_issues(issues):
        for (issue, commits) in issues:
            print(f"- [#{issue['number']}: {issue['title']}]({issue['html_url']})")
            # Not needed usually, useful for investigating why reports were closed.
            # for commit in commits:
            #     print(f"  - [{commit.sha1_short}: {commit.subject}](https://projects.blender.org/blender/blender/commit/{commit.sha1})")

    # Uses 365 for a year, 30 days for a month.
    def as_years_and_months_string(days):
      (years, rem_days) = divmod(days, 365)
      return f"{int(years)} years, {round(rem_days / 30)} months"

    def print_age_stats(oldest, added_age, count):
        print(f"- Oldest: {as_years_and_months_string(oldest)}")
        print(f"- Average age: {as_years_and_months_string(added_age / count)}")

    print("#### Resolved Bugs")
    print_age_stats(oldest_resolved_bug_age, added_age_resolved_bugs, len(resolved_bugs) + len(resolved_high_priority_bugs))
    print("")

    print("**High Severity:**")
    print_issues(resolved_high_priority_bugs)
    print("")
    print("**Other Severities:**")
    print_issues(resolved_bugs)
    print("")

    print("#### Other Closed Bugs (not resolved)")
    print_age_stats(oldest_other_bug_age, added_age_other_bugs, len(other_high_priority_bugs) + len(other_bugs))
    print("")
    print("**High Severity:**")
    print_issues(other_high_priority_bugs)
    print("")
    print("**Other Severities:**")
    print_issues(other_bugs)
    print("")

    print("#### Non-bug Issues:")
    print_age_stats(oldest_non_bug_age, added_age_non_bugs, len(non_bugs))
    print("")
    print_issues(non_bugs)
    print("")


    print(f"Total resolved bugs: {len(resolved_bugs) + len(resolved_high_priority_bugs)}")
    print(f"  -> Out of which high-severity: {len(resolved_high_priority_bugs)}")
    print(f"Total closed bugs: {len(resolved_bugs) + len(resolved_high_priority_bugs) + len(other_high_priority_bugs) + len(other_bugs)}")
    print(f"Total closed non-bugs: {len(non_bugs)}")
    total_closed = len(resolved_bugs) + len(resolved_high_priority_bugs) + len(other_high_priority_bugs) + len(other_bugs) + len(non_bugs)
    print(f"Total closed: {total_closed}")
    print(f"Oldest: {as_years_and_months_string(max(oldest_resolved_bug_age, oldest_other_bug_age, oldest_non_bug_age))}")
    print(f"Average age: {as_years_and_months_string((added_age_resolved_bugs + added_age_other_bugs + added_age_non_bugs) / total_closed)}")

def main() -> None:
    compile_list()


if __name__ == "__main__":
    main()
