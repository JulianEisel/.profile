// ==UserScript==
// @name     Gitea Document Links
// @description Add buttons to put descriptive links to the current page on the clipboard.
// @version  11
// @match    https://projects.blender.org/*
// @grant    GM.setClipboard
// ==/UserScript==

// Based on: https://projects.blender.org/dr.sybren/.profile/src/branch/main/webbrowser-scripts/gitea-document-links.js

function add_document_links() {
  let url_info = find_url_info();
  let id = "";
  let page_name = "";
  let page_title = "";

  if (url_info.issue && url_info.issue != "new") {
    id = url_info.issue;
    page_name = `#${id}`;
    page_title = find_page_title();
  }
  else if (url_info.pull) {
    id = url_info.pull;
    page_name = `#${id}`;
    page_title = find_page_title();
  }
  else if (url_info.commit) {
    id = `${url_info.commit.substring(0, 12)}`;
    page_name = id;
    page_title = find_commit_title();
  }
  else {
    return;
  }

  let gitea_ref = get_gitea_ref(url_info, id);
  if (gitea_ref[0]) {
    add_button("Gitea Ref", `${page_title} (${gitea_ref})`);
    add_button("Gitea Ref (short)", `${gitea_ref}`);
  }

  const url = window.location.href.split('#')[0];
  add_button("MD", `[${page_title}](${url})`);
  add_button("MD (short)", `[${page_name}](${url})`);
  
  add_button("TXT", page_name + ": " + page_title);

  // Buttons at comment area:
  if (url_info.pull) {
    add_comment_button("Build", "@blender-bot build");
    add_comment_button("Package", "@blender-bot package");
  }
}

function add_button(button_text, clipboard_text) {
  const button_style = "padding: 0.4rem; border-color: #265787";
  let button = document.createElement('button');
  button.textContent = button_text;
  button.className = 'ui basic secondary not-in-edit button tiny';
  button.setAttribute("title", clipboard_text);
  button.setAttribute("style", button_style);
  button.addEventListener('click', function() {
    console.log('Clipboard:', clipboard_text);
    GM.setClipboard(clipboard_text);
  });

  let button_parent_elt = find_button_parent_element();
  if (!button_parent_elt) {
      console.log("no parent elt found");
    return;
  }
  button_parent_elt.appendChild(button);
}

function get_gitea_ref(url_info, id) {
  let separator_char = "";
  if (url_info.issue) {
    separator_char = "#";
  }
  else if (url_info.pull) {
    separator_char = "!";
  }
  else if (url_info.commit) {
    separator_char = "@";
  }
  else {
    return "";
  }
  return `${url_info.org}/${url_info.repo}${separator_char}${id}`;
}

function add_comment_button(button_text, comment_text) {
  // Find the parent to insert the button into.
  let form = document.getElementById("comment-form");
  if (!form) return;

  let footer = form.getElementsByClassName("text right")[0];
  if (!footer) return;

  let button = document.createElement("button");
  button.textContent = button_text;
  button.className = "ui secondary button special-greasemonkey-button";
  button.setAttribute("title", comment_text);
  button.addEventListener(
    "click",
    function (event) {
      event.preventDefault();
      event.stopImmediatePropagation();

      console.log("Insert into comment:", comment_text);

      let form = document.getElementById("comment-form");
      if (!form) {
        console.error("cannot find comment form");
        return;
      }

      let textarea = form.getElementsByTagName("textarea")[0];
      if (!textarea) {
        console.error("cannot find text area in form ", form);
        return;
      }

      textarea.value += comment_text + "\n";
    },
    { capture: true }
  );

  footer.insertBefore(button, footer.children[0]);
}

function find_button_parent_element() {
  // For issues & PRs:
  let title_elt = document.getElementById('issue-title-display');
  if (title_elt) {
    // let children = title_elt.getElementsByClassName("edit-button");
    // if (children.length == 0) return undefined;
    // return children[0];
    return title_elt.parentElement;
  }

  // For commits:
  let header_elts = document.getElementsByClassName("ui top commit-header");
  if (header_elts.length > 0) {
    return header_elts[0];
  }
}

function find_page_title() {
  let page_title_elt = document.getElementById("issue-title-display");
  let h1 = page_title_elt.getElementsByTagName("h1")[0];
  let page_title = h1.textContent.trim();
  return page_title
    .replace(/\s*#\d+$/, "")
    .replace(/\s+/, " ")
    .trim();
}

function find_commit_title() {
  let title_elts = document.getElementsByClassName('commit-summary');
  if (title_elts.length == 0) return "";
  return title_elts[0].getAttribute("title");
}

function find_url_info() {
  let parts = window.location.pathname.split('/');
  let url_info = {
    org: "",
    repo: "",
    issue: "",
    pull: "",
    commit: "",
  }
  // 'parts' will be ["", "org", "repo", "bla", "bla"]
  if (parts.length > 1) url_info.org = parts[1];
  if (parts.length > 2) url_info.repo = parts[2];
  if (parts.length > 4) {
    switch (parts[3]) {
      case "issues": url_info.issue = parts[4]; break;
      case "pulls": url_info.pull = parts[4]; break;
      case "commit": url_info.commit = parts[4]; break;
    }
  }
  return url_info;
}

window.setTimeout(add_document_links, 200);
