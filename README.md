## Reports

Weekly reports on work done while under Blender Institute contract:
- [2025](https://projects.blender.org/JulianEisel/.profile/src/branch/main/reports/2025.md)
- [2024](https://projects.blender.org/JulianEisel/.profile/src/branch/main/reports/2024.md)
- [2023 (May - December)](https://projects.blender.org/JulianEisel/Weekly-Reports/src/branch/main/2023.md)
- [2023 (January - April)](https://wiki.blender.org/wiki/User:Severin/Reports/2023)
- [2022](https://wiki.blender.org/wiki/User:Severin/Reports/2022)
- [2021](https://wiki.blender.org/wiki/User:Severin/Reports/2021)
- [2020 (March - December)](https://wiki.blender.org/wiki/User:Severin/Reports/2020)
- [2019/2020 (September 2019 - Mid-March)](https://wiki.blender.org/wiki/User:Severin/Reports/Institute_2019)


## Google Summer of Code

- 2016: [Layer Manager](https://archive.blender.org/wiki/index.php/User:Julianeisel/GSoC-2016/)
- 2019: [Core Support of Virtual Reality Headsets through OpenXR](https://wiki.blender.org/wiki/User:Severin/GSoC-2019/)